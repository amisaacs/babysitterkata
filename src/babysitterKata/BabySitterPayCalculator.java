package babysitterKata;

import java.time.DateTimeException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BabySitterPayCalculator
{
	private String time;
	public int parseHour(String time) {
		
		System.out.print("time: " + time+" hour:");
		int hour = 0;
		Pattern hourPattern = Pattern.compile("[0-9]{1,2}");
		Matcher hourMatch = hourPattern.matcher(time);
		if(hourMatch.find()) {
			hour =  Integer.parseInt(hourMatch.group());
			System.out.println(hour);
			return hour;
		}else
			throw new DateTimeException("Hour is not valid.");
	}
	public String parseMeridian(String time) {
		Pattern meridianPattern = Pattern.compile("[aApP]{1}\\.?[mM]");
		Matcher meridianMatch = meridianPattern.matcher(time);
		if (meridianMatch.find()) {
			return meridianMatch.group();
		}else
			throw new DateTimeException("Meridian is not valid.");
	}

	public boolean isTimeValid(String time)
	{
		time = time.replaceAll("\\s", "");
		
		if(time.equals(""))
			throw new DateTimeException("Time is empty.");
		
		//When hour or meridian is not valid parsing will throw a DateTimeException.
		int hour = parseHour(time);
		String meridian = parseMeridian(time);
		
		if(hour<13 & meridian.length()>0) {
			this.time = time;
			return true;
		}else
			throw new DateTimeException("Time format is invalid.");
	}

	public int convertTo24HourTime(String time)
	{
		isTimeValid(time);
		int hour = parseHour(time);
		String meridian = parseMeridian(time);
		if(meridian.contains("a")||meridian.contains("A"))
			return hour;
		else
			return hour + 12;
	}

}
