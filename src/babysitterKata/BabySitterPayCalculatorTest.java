package babysitterKata;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DateTimeException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class BabySitterPayCalculatorTest
{
	/*
	 * First business rule: Babysitter can only work from 5pm to 4am. 
	 * (Time is rounded up to the nearest hour.)
	 * 
	 * Detail: 
	 * 1. A valid start time is 5pm to 3am. 
	 * 2. An invalid start time is between 4am and 4pm. 
	 * 3. A valid end time is 6pm to 4am AND must be after start time. 
	 * 4. An invalid end time is 5am to 5pm. 
	 * 5. An invalid end time is before the start time.
	 */
	// 'Add leading 0 and leading space to time and refactor isValidTime()'
	@ParameterizedTest
	@ValueSource(strings = {" 6pm", "06am", "7 pm", "7  a m ", "7a m", "8p.m.", "8 a.m.", "10 p. m. ", "11 a.m", "12pm.", "12PM"})
	void validTimeIsValidated(String time) {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		boolean result = calculator.isTimeValid(time);
		assertTrue(result);
	}
	@ParameterizedTest
	@ValueSource(strings = {"", " ", "13am", "15PM", "6km", "6an","7 km", "7 aN", "8 k. p.", "8P.P"}) 
	void invalidTimeThrowsDateTimeException(String time) {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.isTimeValid(time);});
	}
	@Test
	void validTimeIsConvertedTo24hourTime() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		int result = calculator.convertTo24HourTime("6am");
		assertTrue(result==6);
		result = calculator.convertTo24HourTime("6pm");
		assertTrue(result==18);
		result = calculator.convertTo24HourTime("12am");
		assertTrue(result==12);
		result = calculator.convertTo24HourTime("12pm");
		assertTrue(result==24);
	}
	@Test
	void whenConvertingInvalidTimeTo24HourTimeDateTimeExceptionIsThrown() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.convertTo24HourTime("12km");});
	}
	
	/*
	 * Second business rule: 
	 * Babysitter is paid as follows:
	 * 
	 * Family A pays:	$15 per hour before 11pm, and 
	 * 					$20 per hour the rest of the night
	 * 
	 * Family B pays:	$12 per hour before 10pm, 
	 * 					$8 between 10 and 12, and 
	 * 					$16 the rest of the night
	 *
	 * Family C pays:	$21 per hour before 9pm, then 
	 * 					$15 the rest of the night
	 * 
	 * Note: Babysitter can only work for one family a night.
	 */

}
